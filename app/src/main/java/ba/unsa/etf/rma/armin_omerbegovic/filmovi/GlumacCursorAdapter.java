package ba.unsa.etf.rma.armin_omerbegovic.filmovi;

import android.content.Context;
import android.database.Cursor;
import android.support.v4.widget.ResourceCursorAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

/**
 * Created by Armin on 09.06.2017..
 */

public class GlumacCursorAdapter extends ResourceCursorAdapter {

    public GlumacCursorAdapter(Context context, int layout, Cursor c, int flags) {
        super(context, layout, c, flags);

    }


    @Override
    public void bindView(View view, Context context, Cursor cursor) {
        TextView imePrezime = (TextView) view.findViewById(R.id.imePrezime);
        imePrezime.setText(cursor.getString(cursor.getColumnIndex(GlumacDBOpenHelper.GLUMAC_IME)));

        TextView godina = (TextView) view.findViewById(R.id.godinaRodjenja);
        godina.setText(cursor.getString(cursor.getColumnIndex(GlumacDBOpenHelper.GLUMAC_RODJENJE)));

        TextView mjestoRodjenja = (TextView) view.findViewById(R.id.mjestoRodjenja);
        mjestoRodjenja.setText(cursor.getString(cursor.getColumnIndex(GlumacDBOpenHelper.GLUMAC_MJESTO)));

        TextView rating = (TextView) view.findViewById(R.id.rating);
        rating.setText(cursor.getString(cursor.getColumnIndex(GlumacDBOpenHelper.GLUMAC_RATING)));

        ImageView slika = (ImageView) view.findViewById(R.id.slika);
        Picasso.with(context).load(cursor.getColumnIndex(GlumacDBOpenHelper.GLUMAC_SLIKA)).into(slika);
    }
}
