package ba.unsa.etf.rma.armin_omerbegovic.filmovi;

import android.app.Fragment;
import android.content.res.AssetManager;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Locale;

/**
 * Created by Armin on 11.04.2017..
 */

public class FragmentDugmici extends Fragment implements View.OnClickListener {

    private ArrayList<Glumac> glumci;
    /*private ArrayList<String> reziseriLista = new ArrayList<String>(Arrays.asList("Stivene Spielberg", "James Cameron", "Harvey Weinstein","\n" +
            "Graham King","Joel Silver","David Geffen","\n" +
            "Bob Weinstein","Frank Marshall","\n" +
            "Kathleen Kennedy", "Avi Arad"));*/
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
        View v = inflater.inflate(R.layout.fragment_dugmici, container, false);
        Button glumci = (Button) v.findViewById(R.id.glumci);
        Button reziseri = (Button) v.findViewById(R.id.reziseri);
        Button zanrovi = (Button) v.findViewById(R.id.zanrovi);
        Button filmovi = (Button) v.findViewById(R.id.filmovi);
        glumci.setOnClickListener(this);
        reziseri.setOnClickListener(this);
        zanrovi.setOnClickListener(this);
        filmovi.setOnClickListener(this);
        if(Locale.getDefault().getLanguage().toString().equals("en") || Locale.getDefault().getLanguage().toString().equals("eng")){
            ImageView iw = (ImageView)v.findViewById(R.id.jezik);
            iw.setImageResource(R.drawable.uk);
        }
        if(Locale.getDefault().getLanguage().toString().equals("bos") || Locale.getDefault().getLanguage().toString().equals("bs")){
            ImageView iw = (ImageView)v.findViewById(R.id.jezik);
            iw.setImageResource(R.drawable.bh);
        }
        return v;
    }

    @Override
    public void onClick(View v) {
        switch(v.getId()){
            case R.id.reziseri:
                android.app.FragmentManager fm = getFragmentManager();
                FragmentReziseri fr = new FragmentReziseri();
                fm.beginTransaction().replace(R.id.mjestoListaGlumaca, fr).commit();
                break;
            case R.id.glumci:
                android.app.FragmentManager fm1 = getFragmentManager();
                FragmentGlumci fg = new FragmentGlumci();
                fm1.beginTransaction().replace(R.id.mjestoListaGlumaca, fg).commit();
                break;
            case R.id.zanrovi:
                android.app.FragmentManager fm2 = getFragmentManager();
                FragmentZanrovi fz = new FragmentZanrovi();
                fm2.beginTransaction().replace(R.id.mjestoListaGlumaca, fz).commit();
                break;
            case R.id.filmovi:

                android.app.FragmentManager fm3 = getFragmentManager();
                FragmentFilmovi ff = new FragmentFilmovi();
                fm3.beginTransaction().replace(R.id.mjestoListaGlumaca, ff).commit();
                break;
        }
    }
}
