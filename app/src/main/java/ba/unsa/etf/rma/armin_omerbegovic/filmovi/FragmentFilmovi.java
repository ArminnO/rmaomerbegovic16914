package ba.unsa.etf.rma.armin_omerbegovic.filmovi;

import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;

/**
 * Created by Armin on 01.06.2017..
 */

public class FragmentFilmovi extends Fragment implements MyResultReciver.Receiver {
    public static ArrayList<String> filmovi = new ArrayList<>();
    private OnItemClickFilm oic;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View iv = inflater.inflate(R.layout.fragment_filmovi, container, false);

        ListView lista = (ListView) iv.findViewById(R.id.filmoviListView);

        //final MuzicarAdapter adapter;
        ArrayAdapter aa = new ArrayAdapter(this.getActivity(), android.R.layout.simple_list_item_1, filmovi);
        lista.setAdapter(aa);

        final EditText tekst = (EditText) iv.findViewById(R.id.editTextFilm);
        Button dugme = (Button) iv.findViewById(R.id.buttonPretragaFilm);
        dugme.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String query = tekst.getText().toString();

                Intent intent = new Intent(Intent.ACTION_SYNC, null, getActivity(), MyIntentServiceFilmovi.class);
                MyResultReciver mReceiver = new MyResultReciver(new Handler());
                mReceiver.setReceiver(FragmentFilmovi.this);

                intent.putExtra("naziv", query);
                intent.putExtra("receiver", mReceiver);
                getActivity().startService(intent);

                tekst.setText("");

            }
        });

        try {
            //oic definišite kao privatni atribut klase FragmentLista
            //u sljedećoj liniji dohvatamo referencu na roditeljsku aktivnost
            //kako ona implementira interfejs OnItemClick moguće ju je castati u navedeni interfejs
            oic = (OnItemClickFilm) getActivity();
        } catch (ClassCastException e) {
            //u slučaju da se u roditeljskoj aktivnosti nije implementirao interfejs OnItemClick
            //baca se izuzetak
            throw new ClassCastException(getActivity().toString() + "Treba implementirati OnItemClick");
        }
        //ukoliko je aktivnos uspješno cast-ana u interfejs tada njoj prosljeđujemo event
        lista.setOnItemClickListener(new AdapterView.OnItemClickListener(){
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                //poziva se implementirana metoda početne aktivnosti iz interfejsa OnItemClick
                //kao parametar se prosljeđuje pozicija u ListView-u na koju je korisnik kliknuo
                oic.onItemClickedFilm(position);
            }
        });

        return iv;
    }

    @Override
    public void onReceiveResult(int resultCode, Bundle resultData) {
        ListView lista = (ListView)getView().findViewById(R.id.filmoviListView);
        switch (resultCode){
            case MyIntentService.STATUS_RUNNING:
                Toast.makeText(getActivity(), "Service running", Toast.LENGTH_LONG);
                break;

            case MyIntentService.STATUS_FINISHED:
                ArrayList<String> rezultati1;
                rezultati1 = resultData.getStringArrayList("result");
                filmovi = rezultati1;
                //GlumciActivity.glumciLista = rezultati1;
                ArrayAdapter aa = new ArrayAdapter(getActivity(), android.R.layout.simple_list_item_1, rezultati1);

                ListView lista1 = (ListView)getView().findViewById(R.id.filmoviListView);
                lista1.setAdapter(aa);

                Toast.makeText(getActivity(), "Finished", Toast.LENGTH_LONG).show();
                break;

            case MyIntentService.STATUS_ERROR:
                String error = resultData.getString(Intent.EXTRA_TEXT);
                Toast.makeText(getActivity(), error, Toast.LENGTH_LONG).show();
        }
    }

    public interface OnItemClickFilm{
        public void onItemClickedFilm(int pos);
    }

}
