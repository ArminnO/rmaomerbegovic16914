package ba.unsa.etf.rma.armin_omerbegovic.filmovi;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by Armin on 09.06.2017..
 */

public class GlumacDBOpenHelper extends SQLiteOpenHelper {
    public static final String DATABASE_NAME = "glumci.db";
    //glumci
    public static final String DATABASE_TABLE = "Glumci";
    public static final int DATABASE_VERSION = 1;
    public static final String GLUMAC_ID = "_id";
    public static final String GLUMAC_IME = "ime";
    public static final String GLUMAC_RODJENJE = "godinaRodjenja";
    public static final String GLUMAC_SMRT = "godinaSmrti";
    public static final String GLUMAC_BIOGRAFIJA = "biografija";
    public static final String GLUMAC_SLIKA = "slika";
    public static final String GLUMAC_MJESTO = "mjestoRodjenja";
    public static final String GLUMAC_SPOL = "spol";
    public static final String GLUMAC_WEB = "webStranica";
    public static final String GLUMAC_RATING = "rating";
    public static final String GLUMAC_WEBID = "webId";
    //reziseri
    public static final String DATABASE_TABLE2 = "Reziseri";
    public static final String REZISERI_ID = "_id";
    public static final String REZISERI_NAZIV = "ime";
    //reziseri
    public static final String DATABASE_TABLE3 = "Zanrovi";
    public static final String ZANR_ID = "_id";
    public static final String ZANR_NAZIV = "ime";
    public static final String ZANR_SLIKA = "slika";

    private static final String DATABASE_CREATE = "create table " + DATABASE_TABLE + " (" + GLUMAC_ID + " integer primary key autoincrement, " +
            GLUMAC_WEBID + " text not null, " + GLUMAC_IME + " text not null, " + GLUMAC_RODJENJE + " text not null, " +
            GLUMAC_SMRT + " text not null, " + GLUMAC_BIOGRAFIJA + " text not null, " + GLUMAC_SLIKA + " text not null, " +
            GLUMAC_MJESTO + " text not null, " + GLUMAC_SPOL + " text not null, " + GLUMAC_WEB + " text not null, " +
            GLUMAC_RATING + " text not null);";

    private static final String DATABASE_CREATE2 = "create table " + DATABASE_TABLE2 + " (" + REZISERI_ID + " integer primary key autoincrement, " +
            REZISERI_NAZIV + " text not null);";

    private static final String DATABASE_CREATE3 = "create table " + DATABASE_TABLE3 + " (" + ZANR_ID + " integer primary key autoincrement, " +
            ZANR_NAZIV + " text not null, " + ZANR_SLIKA + " text not null);";


    public GlumacDBOpenHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        db.execSQL(DATABASE_CREATE);
        db.execSQL(DATABASE_CREATE2);
        db.execSQL(DATABASE_CREATE3);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF IT EXISTS " + DATABASE_TABLE);
        db.execSQL("DROP TABLE IF IT EXISTS " + DATABASE_TABLE2);
        db.execSQL("DROP TABLE IF IT EXISTS " + DATABASE_TABLE3);
        onCreate(db);
    }

}