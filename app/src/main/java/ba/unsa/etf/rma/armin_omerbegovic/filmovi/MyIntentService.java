package ba.unsa.etf.rma.armin_omerbegovic.filmovi;

import android.app.IntentService;
import android.content.Intent;
import android.os.Bundle;
import android.os.ResultReceiver;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;



/**
 * Created by Armin on 27.05.2017..
 */

public class MyIntentService extends IntentService {
    public static final int STATUS_RUNNING = 0;
    public static final int STATUS_FINISHED = 1;
    public static final int STATUS_ERROR = 2;


    ArrayList<Glumac> rez = new ArrayList<>();

    public MyIntentService() {
        super(null);
    }

    public MyIntentService(String name) {
        super(name);
        // Sav posao koji treba da obavi konstruktor treba da se
        // nalazi ovdje
    }

    @Override
    public void onCreate() {
        super.onCreate();
        // Akcije koje se trebaju obaviti pri kreiranju servisa
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        // Kod koji se nalazi ovdje će se izvršavati u posebnoj niti
        // Ovdje treba da se nalazi funkcionalnost servisa koja je
        // vremenski zahtjevna

        final ResultReceiver receiver = intent.getParcelableExtra("receiver");
        Bundle bundle = new Bundle();

        /* Update UI: Početak taska */
        receiver.send(STATUS_RUNNING, Bundle.EMPTY);

        String query = intent.getStringExtra("ime");


        //String url1 = "https://api.spotify.com/v1/search?q=" + query + "&type=artist";
        String url1 = "http://api.themoviedb.org/3/search/person?api_key=80813ae5c5a1696951af1e3343f3c95e&query=" + query;

        try {
            URL url = new URL(url1);
            HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
            InputStream in = new BufferedInputStream(urlConnection.getInputStream());
            String rezultat = convertStreamToString(in);
            JSONObject jo = new JSONObject(rezultat);
            //JSONObject artists = jo.getJSONObject("results");
            JSONArray items = jo.getJSONArray("results");

            for (int i = 0; i < items.length(); i++) {
                JSONObject artist = items.getJSONObject(i);
                //String name = artist.getString("name");
                String actor_ID = artist.getString("id");
                //String birthday = artist.getString("birthday");
                String url2 = "https://api.themoviedb.org/3/person/"+actor_ID+"?api_key=80813ae5c5a1696951af1e3343f3c95e&language=en-US";
                URL urll = new URL(url2);
                HttpURLConnection urlConnection1 = (HttpURLConnection) urll.openConnection();
                InputStream in1 = new BufferedInputStream(urlConnection1.getInputStream());
                String rezultat1 = convertStreamToString(in1);
                JSONObject jo1 = new JSONObject(rezultat1);
                String name = jo1.getString("name");
                String birthday = jo1.getString("birthday");
                String deathday = jo1.getString("deathday");
                String biography = jo1.getString("biography");
                String picture = "http://image.tmdb.org/t/p/w185/" + jo1.getString("profile_path");
                String placeOfBirth = jo1.getString("place_of_birth");
                String gender = jo1.getString("gender");
                String web = "http://www.imdb.com/name/" + jo1.getString("imdb_id");
                String rating = jo1.getString("popularity");


                rez.add(new Glumac(actor_ID, name, birthday, deathday, biography, picture, placeOfBirth, gender, web, rating));
            }
            //Proslijedi rezultate nazad u pozivatelja

            bundle.putParcelableArrayList("result", rez);

            receiver.send(STATUS_FINISHED, bundle);

        } catch (ClassCastException e) {
        /* Vrati obavijest da je došlo do izuzetka, e – izuzetak */
            bundle.putString(Intent.EXTRA_TEXT, e.toString());
            receiver.send(STATUS_ERROR, bundle);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public String convertStreamToString(InputStream is) {
        BufferedReader reader = new BufferedReader(new InputStreamReader(is));
        StringBuilder sb = new StringBuilder();
        String line = null;
        try {
            while ((line = reader.readLine()) != null) {
                sb.append(line + "\n");
            }
        } catch (IOException e) {
        } finally {
            try {
                is.close();
            } catch (IOException e) {
            }
        }
        return sb.toString();
    }
}
