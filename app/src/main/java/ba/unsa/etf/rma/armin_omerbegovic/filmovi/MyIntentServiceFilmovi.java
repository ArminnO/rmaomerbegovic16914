package ba.unsa.etf.rma.armin_omerbegovic.filmovi;

import android.app.IntentService;
import android.content.Intent;
import android.os.Bundle;
import android.os.ResultReceiver;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

/**
 * Created by Armin on 01.06.2017..
 */

public class MyIntentServiceFilmovi extends IntentService {
    public static final int STATUS_RUNNING = 0;
    public static final int STATUS_FINISHED = 1;
    public static final int STATUS_ERROR = 2;


    ArrayList<String> rez = new ArrayList<>();

    public MyIntentServiceFilmovi() {
        super(null);
    }

    public MyIntentServiceFilmovi(String name) {
        super(name);
        // Sav posao koji treba da obavi konstruktor treba da se
        // nalazi ovdje
    }

    @Override
    public void onCreate() {
        super.onCreate();
        // Akcije koje se trebaju obaviti pri kreiranju servisa
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        // Kod koji se nalazi ovdje će se izvršavati u posebnoj niti
        // Ovdje treba da se nalazi funkcionalnost servisa koja je
        // vremenski zahtjevna

        final ResultReceiver receiver = intent.getParcelableExtra("receiver");
        Bundle bundle = new Bundle();

        /* Update UI: Početak taska */
        receiver.send(STATUS_RUNNING, Bundle.EMPTY);

        String query = intent.getStringExtra("naziv");


        //String url1 = "https://api.spotify.com/v1/search?q=" + query + "&type=artist";
        String url1 = "https://api.themoviedb.org/3/search/movie?api_key=80813ae5c5a1696951af1e3343f3c95e&query=" + query;

        try {
            URL url = new URL(url1);
            HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
            InputStream in = new BufferedInputStream(urlConnection.getInputStream());
            String rezultat = convertStreamToString(in);
            JSONObject jo = new JSONObject(rezultat);
            //JSONObject artists = jo.getJSONObject("results");
            JSONArray items = jo.getJSONArray("results");
            for (int i = 0; i < items.length(); i++) {
                JSONObject movie = items.getJSONObject(i);
                //String name = artist.getString("name");
                String title = movie.getString("title");
                //String birthday = artist.getString("birthday");
                rez.add(title);
            }
            //Proslijedi rezultate nazad u pozivatelja

            bundle.putStringArrayList("result", rez);

            receiver.send(STATUS_FINISHED, bundle);

        } catch (ClassCastException e) {
        /* Vrati obavijest da je došlo do izuzetka, e – izuzetak */
            bundle.putString(Intent.EXTRA_TEXT, e.toString());
            receiver.send(STATUS_ERROR, bundle);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public String convertStreamToString(InputStream is) {
        BufferedReader reader = new BufferedReader(new InputStreamReader(is));
        StringBuilder sb = new StringBuilder();
        String line = null;
        try {
            while ((line = reader.readLine()) != null) {
                sb.append(line + "\n");
            }
        } catch (IOException e) {
        } finally {
            try {
                is.close();
            } catch (IOException e) {
            }
        }
        return sb.toString();
    }
}
