package ba.unsa.etf.rma.armin_omerbegovic.filmovi;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

/**
 * Created by Armin on 14.04.2017..
 */

public class FragmentDugmici500 extends Fragment implements View.OnClickListener{
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
        View v = inflater.inflate(R.layout.fragment_dugmici_500, container, false);
        Button glumci500 = (Button) v.findViewById(R.id.glumci500);
        Button ostalo = (Button) v.findViewById(R.id.ostalo);
        glumci500.setOnClickListener(this);
        ostalo.setOnClickListener(this);
        return v;
    }

    @Override
    public void onClick(View v) {
        switch(v.getId()){
            case R.id.glumci500:
                android.app.FragmentManager fm1 = getFragmentManager();
                FragmentGlumci fg = new FragmentGlumci();
                fm1.beginTransaction().replace(R.id.mjestoListaGlumaca, fg).commit();
                FragmentDetalji fd = new FragmentDetalji();
                fm1.beginTransaction().replace(R.id.mjestoDetalji, fd).commit();
                break;
            case R.id.ostalo:
                android.app.FragmentManager fm2 = getFragmentManager();
                FragmentZanrovi fz = new FragmentZanrovi();
                fm2.beginTransaction().replace(R.id.mjestoListaGlumaca, fz).commit();
                FragmentReziseri fr = new FragmentReziseri();
                fm2.beginTransaction().replace(R.id.mjestoDetalji, fr).commit();
                break;
        }

    }
}
