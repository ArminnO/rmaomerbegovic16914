package ba.unsa.etf.rma.armin_omerbegovic.filmovi;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Armin on 22.03.2017..
 */

public class GlumacAdapter extends ArrayAdapter<Glumac> {
    private Context context;
    private List<Glumac> glumciLista;

    public GlumacAdapter(Context context, int resource, ArrayList<Glumac> objects){
        super(context, resource, objects);

        this.context = context;
        this.glumciLista = objects;
    }

    public View getView(int position, View convertView, ViewGroup parent){
        Glumac glumac = glumciLista.get(position);

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.glumac_lista, null);

        TextView imePrezime = (TextView) view.findViewById(R.id.imePrezime);
        TextView godinaRodjenja = (TextView) view.findViewById(R.id.godinaRodjenja);
        TextView mjestoRodjenja = (TextView) view.findViewById(R.id.mjestoRodjenja);
        TextView rating = (TextView) view.findViewById(R.id.rating);
        ImageView slika = (ImageView) view.findViewById(R.id.slika);

        imePrezime.setText(glumac.getImePrezime());
        godinaRodjenja.setText(glumac.getGodinaRodjenja());
        mjestoRodjenja.setText(glumac.getMjestoRodjenja());
        rating.setText(glumac.getRating());
        //slika.setImageResource(glumac.getSlika());
        Picasso.with(getContext()).load(glumac.getSlika()).into(slika);

        return view;
    }
}
