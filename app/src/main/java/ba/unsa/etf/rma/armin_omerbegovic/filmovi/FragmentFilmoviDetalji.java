package ba.unsa.etf.rma.armin_omerbegovic.filmovi;

import android.Manifest;
import android.app.Fragment;
import android.app.admin.DeviceAdminInfo;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.provider.CalendarContract;
import android.support.v4.app.ActivityCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import java.util.Calendar;

import static ba.unsa.etf.rma.armin_omerbegovic.filmovi.R.id.nazivFilma;

/**
 * Created by Armin on 01.06.2017..
 */

public class FragmentFilmoviDetalji extends Fragment {
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_filmovi_detalji, container, false);

        if (getArguments() != null && getArguments().containsKey("filmovi")) {
            String film = getArguments().getString("filmovi");

            TextView nazivInfo = (TextView) v.findViewById(nazivFilma);
            nazivInfo.setText(film);
            Button zapamti = (Button) v.findViewById(R.id.zapamti);

            DatePicker dp = (DatePicker) v.findViewById(R.id.datum);
            final int godina = dp.getYear();
            final int mjesec = dp.getMonth();
            final int dan = dp.getDayOfMonth();
            zapamti.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    long calID = 1;
                    long startMillis = 0;
                    long endMillis = 0;
                    Calendar beginTime = Calendar.getInstance();
                    beginTime.set(godina, mjesec, dan);
                    startMillis = beginTime.getTimeInMillis();
                    Calendar endTime = Calendar.getInstance();
                    endTime.set(godina, mjesec, dan);
                    endMillis = endTime.getTimeInMillis();

                    ContentResolver cr = getActivity().getContentResolver();
                    ContentValues values = new ContentValues();
                    values.put(CalendarContract.Events.DTSTART, startMillis);
                    values.put(CalendarContract.Events.DTEND, endMillis);
                    values.put(CalendarContract.Events.TITLE, nazivFilma);
                    values.put(CalendarContract.Events.DESCRIPTION, "Watch watch");
                    values.put(CalendarContract.Events.CALENDAR_ID, calID);
                    values.put(CalendarContract.Events.EVENT_TIMEZONE, "Etc/GMT+1");
                    if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.WRITE_CALENDAR) != PackageManager.PERMISSION_GRANTED) {
                        // TODO: Consider calling
                        //    ActivityCompat#requestPermissions
                        // here to request the missing permissions, and then overriding
                        //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                        //                                          int[] grantResults)
                        // to handle the case where the user grants the permission. See the documentation
                        // for ActivityCompat#requestPermissions for more details.
                        return;
                    }
                    Uri uri = cr.insert(CalendarContract.Events.CONTENT_URI, values);

// get the event ID that is the last element in the Uri
                    Toast.makeText(getActivity(), "Film zapamcen", Toast.LENGTH_LONG).show();
//
                }
            });

        }


        return v;
    }
}
