package ba.unsa.etf.rma.armin_omerbegovic.filmovi;

import android.app.Fragment;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CursorAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import static ba.unsa.etf.rma.armin_omerbegovic.filmovi.GlumacDBOpenHelper.DATABASE_TABLE;
import static ba.unsa.etf.rma.armin_omerbegovic.filmovi.GlumacDBOpenHelper.GLUMAC_IME;

/**
 * Created by Armin on 11.04.2017..
 */

public class FragmentGlumci extends Fragment implements MyResultReciver.Receiver {
    private ArrayList<Glumac> glumci = new ArrayList<>();
    GlumacAdapter aa;
    OnItemClick oic;
    ListView lista;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        /*glumciLista.add(new Glumac("Will Smith", "1968", "0", "Willard Carroll \"Will\" Smith, Jr. (born September 25, 1968) is an American actor, comedian, producer, rapper, and songwriter. He has enjoyed success in television, film, and music. In April 2007, Newsweek called him \"the most powerful actor in Hollywood\". Smith has been nominated for five Golden Globe Awards, two Academy Awards, and has won four Grammy Awards.\n" +
                "\n" +
                "In the late 1980s, Smith achieved modest fame as a rapper under the name The Fresh Prince. In 1990, his popularity increased dramatically when he starred in the popular television series The Fresh Prince of Bel-Air. The show ran for six seasons (1990-96) on NBC and has been syndicated consistently on various networks since then. After the series ended, Smith moved from television to film, and ultimately starred in numerous blockbuster films. He is the only actor to have eight consecutive films gross over $100 million in the domestic box office, eleven consecutive films gross over $150 million internationally, and eight consecutive films in which he starred open at the number one spot in the domestic box office tally.",
                R.drawable.will, "Pennsylvania, USA", "Musko", "http://www.imdb.com/name/nm0000226/", "5"));
        glumciLista.add(new Glumac("Denzel Washington", "1954", "0", "Denzel Washington is an American actor and filmmaker. He has received three Golden Globe awards, a Tony Award, and two Academy Awards: Best Supporting Actor for the historical war drama film Rat za slavu (1989) and Best Actor for his role as a corrupt cop in the crime thriller Dan obuke (2001).\n" +
                "\n" +
                "Denzel Hayes Washington, Jr. was born on December 28, 1954 in Mount Vernon, New York. He is the middle of three children of a beautician mother, Lennis (Lowe), from Georgia, and a Pentecostal minister father, Denzel Washington, Sr., from Virginia. After graduating from high school, Denzel enrolled at Fordham University, intent on a career in journalism. However, he caught the acting bug while appearing in student drama productions and, upon graduation, he moved to San Francisco and enrolled at the American Conservatory Theater. He left A.C.T. after only one year to seek work as an actor. His first paid acting role was in a summer stock theater stage production in St. Mary's City, Maryland. The play was \"Wings of the Morning\", which is about the founding of the colony of Maryland (now the state of Maryland) and the early days of the Maryland colonial assembly (a legislative body). He played the part of a real historical character, Mathias Da Sousa, although much of the dialogue was created. Afterwards he began to pursue screen roles in earnest. With his acting versatility and powerful sexual presence, he had no difficulty finding work in numerous television productions.",
                R.drawable.danzel, "New York, USA", "Musko", "www.imdb.com/name/nm0000243/", "5"));
        glumciLista.add(new Glumac("Robert Downey Jr.", "1965", "0", "Robert Downey Jr. has evolved into one of the most respected actors in Hollywood. With an amazing list of credits to his name, he has managed to stay new and fresh even after over four decades in the business.\n" +
                "\n" +
                "Downey was born April 4, 1965 in Manhattan, New York, the son of writer, director and filmographer Robert Downey Sr. and actress Elsie Downey (née Elsie Ann Ford). Robert's father is of half Lithuanian Jewish, one quarter Hungarian Jewish, and one quarter Irish, descent, while Robert's mother was of English, Scottish, German, and Swiss-German ancestry. Robert and his sister, Allyson Downey, were immersed in film and the performing arts from a very young age, leading Downey Jr. to study at the Stagedoor Manor Performing Arts Training Center in upstate New York, before moving to California with his father following his parents' 1978 divorce.Downey was born April 4, 1965 in Manhattan, New York, the son of writer, director and filmographeDowney was born April 4, 1965 in Manhattan, New York, the son of writer, director and filmographeDowney was born April 4, 1965 in Manhattan, New York, the son of writer, director and filmographe",
                R.drawable.robert, "New York, USA", "Musko", "http://www.imdb.com/name/nm0000375", "5"));
        glumciLista.add(new Glumac("Marlon Brando", "1924", "2004", "Marlon Brando is widely considered the greatest movie actor of all time, rivaled only by the more theatrically oriented Laurence Olivier in terms of esteem. Unlike Olivier, who preferred the stage to the screen, Brando concentrated his talents on movies after bidding the Broadway stage adieu in 1949, a decision for which he was severely criticized when his star began to dim in the 1960s and he was excoriated for squandering his talents. No actor ever exerted such a profound influence on succeeding generations of actors as did Brando. More than 50 years after he first scorched the screen as Stanley Kowalski in the movie version of Tennessee Williams' A Streetcar Named Desire (1951) and a quarter-century after his last great performance as Col. Kurtz in Francis Ford Coppola's Apokalipsa danas (1979), all American actors are still being measured by the yardstick that was Brando. It was if the shadow of John Barrymore, the great American actor closest to Brando in terms of talent and stardom, dominated the acting field up until the 1970s. He did not, nor did any other actor so dominate the public's consciousness of what WAS an actor before or since Brando's 1951 on-screen portrayal of Stanley made him a cultural icon. Brando eclipsed the reputation of other great actors circa 1950, such as Paul Muni and Fredric March. Only the luster of Spencer Tracy's reputation hasn't dimmed when seen in the starlight thrown off by Brando. However, neither Tracy nor Olivier created an entire school of acting just by the force of his personality. Brando did.",
                R.drawable.marlon, "Los Angeles, USA", "Musko", "http://www.imdb.com/name/nm0000008/", "5"));
        glumciLista.add(new Glumac("Scarlett Johansson", "1984", "0", "Scarlett Ingrid Johansson was born in New York City. Her mother, Melanie Sloan, is from a Jewish family from the Bronx, and her father, Karsten Johansson, is a Danish-born architect, from Copenhagen. She has a sister, Vanessa Johansson, who is also an actress, a brother, Adrian, a twin brother, Hunter Johansson, born three minutes after her, and a paternal half-brother, Christian. Her grandfather was writer Ejner Johansson.\n" +
                "\n" +
                "Johansson began acting during childhood, after her mother started taking her to auditions. She made her professional acting debut at the age of eight in the off-Broadway production of \"Sophistry\" with Ethan Hawke, at New York's Playwrights Horizons. She would audition for commercials but took rejection so hard her mother began limiting her to film tryouts. She made her film debut at the age of nine, as John Ritter's character's daughter in the fantasy comedy North (1994). Following minor roles in Opravdana sumnja (1995), as the daughter of Sean Connery and Kate Capshaw's character, and If Lucy Fell (1996), she played the role of Amanda in Manny & Lo (1996). Her performance in Manny & Lo garnered a nomination for the Independent Spirit Award for Best Lead Female, and positive reviews, one noting, \"[the film] grows on you, largely because of the charm of ... Scarlett Johansson\", while San Francisco Chronicle critic Mick LaSalle commentated on her \"peaceful aura\", and wrote, \"If she can get through puberty with that aura undisturbed, she could become an important actress.\"."
                , R.drawable.scarlett, "New York, USA", "Zensko", "http://www.imdb.com/name/nm0424060", "5"));
        glumciLista.add(new Glumac("Leonardo DiCaprio", "1974", "0", "Few actors in the world have had a career quite as diverse as Leonardo DiCaprio's. DiCaprio has gone from relatively humble beginnings, as a supporting cast member of the sitcom Growing Pains (1985) and low budget horror movies, such as Critters 3 (1991), to a major teenage heartthrob in the 1990s, as the hunky lead actor in movies such as Romeo i Julija (1996) and Titanik (1997), to then become a leading man in Hollywood blockbusters, made by internationally renowned directors such as Martin Scorsese and Christopher Nolan.\n" +
                "\n" +
                "Leonardo Wilhelm DiCaprio was born November 11, 1974 in Los Angeles, California, the only child of Irmelin DiCaprio (née Indenbirken) and former comic book artist George DiCaprio. His father is of Italian and German descent, and his mother, who is German-born, is of German and Russian ancestry. His middle name, \"Wilhelm\", was his maternal grandfather's first name. Leonardo's father had achieved minor status as an artist and distributor of cult comic book titles, and was even depicted in several issues of American Splendor, the cult semiautobiographical comic book series by the late 'Harvey Pekar', a friend of George's. Leonardo's performance skills became obvious to his parents early on, and after signing him up with a talent agent who wanted Leonardo to perform under the stage name \"Lenny Williams\", DiCaprio began appearing on a number of television commercials and educational programs.",
                R.drawable.leonardo, "Los Angeles, USA", "Musko", "http://www.imdb.com/name/nm0000138/", "5"));
        glumci = glumciLista;*/
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
        final View iv = inflater.inflate(R.layout.fragment_glumci,container,false);

        final ListView lista = (ListView)iv.findViewById(R.id.glumciListView);

        //final MuzicarAdapter adapter;
        aa = new GlumacAdapter(this.getActivity(),R.layout.glumac_lista, glumci);


        final EditText tekst = (EditText)iv.findViewById(R.id.editTextIme);
        Button dugme = (Button)iv.findViewById(R.id.buttonPretraga);
        dugme.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String query = tekst.getText().toString();
                if(query.contains("actor:")){
                    GlumacDBOpenHelper helper = new GlumacDBOpenHelper(getActivity(), GlumacDBOpenHelper.DATABASE_NAME,null, 1);
                    SQLiteDatabase db = helper.getReadableDatabase();
                    String imePretraga = query.toString().substring(6);
                    String countQuery = "SELECT * FROM " + DATABASE_TABLE + " WHERE " +
                            GlumacDBOpenHelper.GLUMAC_IME + " LIKE '" + imePretraga + "%';";
                    Cursor cursor = db.rawQuery(countQuery, null);
                    int i = cursor.getColumnIndexOrThrow(GLUMAC_IME);
                    while (cursor.moveToNext()){
                        Log.d("Ime: ", cursor.getString(i));
                    }

                    GlumacCursorAdapter gc = new GlumacCursorAdapter(getActivity(), R.layout.glumac_lista, cursor, 0);
                    lista.setAdapter(gc);
                    //cursor.close();

                    Toast.makeText(getActivity(), "sadrzi", Toast.LENGTH_LONG).show();
                }
                else {
                    Intent intent = new Intent(Intent.ACTION_SYNC, null, getActivity(), MyIntentService.class);
                    MyResultReciver mReceiver = new MyResultReciver(new Handler());
                    mReceiver.setReceiver(FragmentGlumci.this);
                    intent.putExtra("ime", query);
                    intent.putExtra("receiver", mReceiver);
                    getActivity().startService(intent);
                    tekst.setText("");
                    lista.setAdapter(aa);
                }

            }
        });

        try {
            //oic definišite kao privatni atribut klase FragmentLista
            //u sljedećoj liniji dohvatamo referencu na roditeljsku aktivnost
            //kako ona implementira interfejs OnItemClick moguće ju je castati u navedeni interfejs
            oic = (OnItemClick)getActivity();
        } catch (ClassCastException e) {
            //u slučaju da se u roditeljskoj aktivnosti nije implementirao interfejs OnItemClick
            //baca se izuzetak
            throw new ClassCastException(getActivity().toString() + "Treba implementirati OnItemClick");
        }
        //ukoliko je aktivnos uspješno cast-ana u interfejs tada njoj prosljeđujemo event
        lista.setOnItemClickListener(new AdapterView.OnItemClickListener(){
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                //poziva se implementirana metoda početne aktivnosti iz interfejsa OnItemClick
                //kao parametar se prosljeđuje pozicija u ListView-u na koju je korisnik kliknuo
                oic.onItemClicked(position);
            }
        });

        return iv;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState){
        super.onActivityCreated(savedInstanceState);
            /*ListView lv = (ListView)getView().findViewById(R.id.glumciListView);
            GlumacAdapter ga = new GlumacAdapter(getActivity(), R.layout.glumac_lista, glumci);
            lv.setAdapter(ga);
            try{
                oic = (OnItemClick)getActivity();
            }catch(ClassCastException e){
                throw new ClassCastException(getActivity().toString() + "Treba implementirati onItemClick");
            }
        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                oic.onItemClicked(position);
            }
        });*/
    }

    @Override
    public void onReceiveResult(int resultCode, Bundle resultData) {
        lista = (ListView)getView().findViewById(R.id.glumciListView);
        switch (resultCode){
            case MyIntentService.STATUS_RUNNING:
                Toast.makeText(getActivity(), "Service running", Toast.LENGTH_LONG);
                break;

            case MyIntentService.STATUS_FINISHED:
                ArrayList<Glumac> rezultati1;
                rezultati1 = resultData.getParcelableArrayList("result");
                GlumciActivity.glumciLista = rezultati1;
                GlumacAdapter aa = new GlumacAdapter(getActivity(), R.layout.glumac_lista, rezultati1);

                ListView lista1 = (ListView)getView().findViewById(R.id.glumciListView);
                lista1.setAdapter(aa);

                Toast.makeText(getActivity(), "Finished", Toast.LENGTH_LONG).show();
                break;

            case MyIntentService.STATUS_ERROR:
                String error = resultData.getString(Intent.EXTRA_TEXT);
                Toast.makeText(getActivity(), error, Toast.LENGTH_LONG).show();
        }
    }

    public interface OnItemClick{
        public void onItemClicked(int pos);
    }
}
