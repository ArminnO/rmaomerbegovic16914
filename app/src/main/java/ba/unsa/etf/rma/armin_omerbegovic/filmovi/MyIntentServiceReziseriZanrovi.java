package ba.unsa.etf.rma.armin_omerbegovic.filmovi;

import android.app.IntentService;
import android.content.Intent;
import android.os.Bundle;
import android.os.ResultReceiver;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

/**
 * Created by Armin on 29.05.2017..
 */

public class MyIntentServiceReziseriZanrovi extends IntentService {
    public static final int STATUS_RUNNING = 0;
    public static final int STATUS_FINISHED = 1;
    public static final int STATUS_ERROR = 2;


    public static ArrayList<String> rez = new ArrayList<>();
    public static ArrayList<Zanr> rez2 = new ArrayList<>();

    public MyIntentServiceReziseriZanrovi() {
        super(null);
    }

    public MyIntentServiceReziseriZanrovi(String name) {
        super(name);
        // Sav posao koji treba da obavi konstruktor treba da se
        // nalazi ovdje
    }

    @Override
    public void onCreate() {
        super.onCreate();
        // Akcije koje se trebaju obaviti pri kreiranju servisa
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        // Kod koji se nalazi ovdje će se izvršavati u posebnoj niti
        // Ovdje treba da se nalazi funkcionalnost servisa koja je
        // vremenski zahtjevna

        final ResultReceiver receiver = intent.getParcelableExtra("receiver1");
        Bundle bundle = new Bundle();

        /* Update UI: Početak taska */
        receiver.send(STATUS_RUNNING, Bundle.EMPTY);

        String queryy = intent.getStringExtra("id");


        //String url1 = "https://api.spotify.com/v1/search?q=" + query + "&type=artist";
        String url1 = "http://api.themoviedb.org/3/discover/movie?api_key=80813ae5c5a1696951af1e3343f3c95e&with_cast=" + queryy + "&sort_by=primary_release_data.desc";

        try {
            rez.clear();
            rez2.clear();
            URL url = new URL(url1);
            HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
            InputStream in = new BufferedInputStream(urlConnection.getInputStream());
            String rezultat = convertStreamToString(in);
            JSONObject jo = new JSONObject(rezultat);
            //JSONObject artists = jo.getJSONObject("results");
            JSONArray items = jo.getJSONArray("results");
            Boolean ima = false;
            for (int i = 0; i < items.length(); i++) {
                JSONObject actor = items.getJSONObject(i);
                //String name = artist.getString("name");
                String actor_ID = actor.getString("id");
                //String birthday = artist.getString("birthday");
                String url2 = "http://api.themoviedb.org/3/movie/" + actor_ID +"/credits?api_key=80813ae5c5a1696951af1e3343f3c95e";
                URL urll = new URL(url2);
                HttpURLConnection urlConnection1 = (HttpURLConnection) urll.openConnection();
                InputStream in1 = new BufferedInputStream(urlConnection1.getInputStream());
                String rezultat1 = convertStreamToString(in1);
                JSONObject jo1 = new JSONObject(rezultat1);
                JSONArray producers = jo1.getJSONArray("crew");
                for(int j = 0; j < producers.length(); j++){
                    if(rez.size() == 7){
                        break;
                    }
                    JSONObject job = producers.getJSONObject(j);
                    String isProducer = job.getString("job");
                    if(isProducer.equalsIgnoreCase("Director")) {
                        String producerName = job.getString("name");
                        if(!rez.contains(producerName))
                            rez.add(producerName);

                    }
                }

                String url23 = "https://api.themoviedb.org/3/movie/" + actor_ID + "?api_key=80813ae5c5a1696951af1e3343f3c95e&language=en-US";
                URL urlll = new URL(url23);
                HttpURLConnection urlConnection11 = (HttpURLConnection) urlll.openConnection();
                InputStream in11 = new BufferedInputStream(urlConnection11.getInputStream());
                String rezultat11 = convertStreamToString(in11);
                JSONObject jo11 = new JSONObject(rezultat11);
                JSONArray genres = jo11.getJSONArray("genres");
                for(int j = 0; j < genres.length(); j++){
                    if(rez2.size() == 7){
                        break;
                    }
                    JSONObject zanr = genres.getJSONObject(j);
                    String ime = zanr.getString("name");
                    if(!provjeri(rez2, ime)){
                        rez2.add(new Zanr(ime));
                    }

                }
            }

            //Proslijedi rezultate nazad u pozivatelja
            FragmentDetalji.producenti = rez;

            Log.d("AAAAAAAAAAAAAAAAA", String.valueOf(rez2.size()));
            FragmentDetalji.zanrovi = rez2;
            //bundle.putStringArrayList("resultProducer", rez);

            receiver.send(STATUS_FINISHED, bundle);

        } catch (ClassCastException e) {
        /* Vrati obavijest da je došlo do izuzetka, e – izuzetak */
            bundle.putString(Intent.EXTRA_TEXT, e.toString());
            receiver.send(STATUS_ERROR, bundle);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
    public Boolean provjeri(ArrayList<Zanr> lista, String naziv){
        for(Zanr t : lista){
            if(t.getNaziv().equals(naziv)){
                return true;
            }
        }
        return false;
    }
    public String convertStreamToString(InputStream is) {
        BufferedReader reader = new BufferedReader(new InputStreamReader(is));
        StringBuilder sb = new StringBuilder();
        String line = null;
        try {
            while ((line = reader.readLine()) != null) {
                sb.append(line + "\n");
            }
        } catch (IOException e) {
        } finally {
            try {
                is.close();
            } catch (IOException e) {
            }
        }
        return sb.toString();
    }
}
