package ba.unsa.etf.rma.armin_omerbegovic.filmovi;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Armin on 22.03.2017..
 */

public class ZanrAdapter extends ArrayAdapter<Zanr> {
    private Context context;
    private List<Zanr> zanroviLista;

    public ZanrAdapter(Context context, int resource, ArrayList<Zanr> objects){
        super(context, resource, objects);

        this.context = context;
        this.zanroviLista = objects;
    }

    public View getView(int position, View convertView, ViewGroup parent){
        Zanr zanr = zanroviLista.get(position);

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.zanr_lista, null);

        TextView naziv = (TextView) view.findViewById(R.id.nazivZanra);
        ImageView slika = (ImageView) view.findViewById(R.id.slikaZanra);

        naziv.setText(zanr.getNaziv());
        if(zanr.getNaziv().equals("Action")) {
            slika.setImageResource(R.drawable.action);
        }
        if(zanr.getNaziv().equals("Adventure")) {
            slika.setImageResource(R.drawable.adventure);
        }
        if(zanr.getNaziv().equals("Horror")) {
            slika.setImageResource(R.drawable.horror);
        }
        if(zanr.getNaziv().equals("Thriller")) {
            slika.setImageResource(R.drawable.zhriller);
        }
        if(zanr.getNaziv().equals("Comedy")) {
            slika.setImageResource(R.drawable.comedy);
        }
        if(zanr.getNaziv().equals("Science Fiction")) {
            slika.setImageResource(R.drawable.science);
        }
        if(zanr.getNaziv().equals("Drama")) {
            slika.setImageResource(R.drawable.drama);
        }
        if(zanr.getNaziv().equals("Fantasy")) {
            slika.setImageResource(R.drawable.fantasy);
        }
        if(zanr.getNaziv().equals("Crime")) {
            slika.setImageResource(R.drawable.crime);
        }
        if(zanr.getNaziv().equals("Western")) {
            slika.setImageResource(R.drawable.western);
        }
        return view;
    }
}
