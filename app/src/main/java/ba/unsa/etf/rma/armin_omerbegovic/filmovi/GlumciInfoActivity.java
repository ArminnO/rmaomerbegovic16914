package ba.unsa.etf.rma.armin_omerbegovic.filmovi;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import org.w3c.dom.Text;

import java.net.URL;
import java.net.URLConnection;

public class GlumciInfoActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_glumci_info);

        TextView nazivInfo = (TextView) findViewById(R.id.imePrezime);
        TextView rodjenjeInfo = (TextView) findViewById(R.id.godinaRodjenja);
        TextView smrtInfo = (TextView) findViewById(R.id.godinaSmrti);
        TextView mjestoInfo = (TextView) findViewById(R.id.mjestoRodjenja);
        TextView spolInfo = (TextView) findViewById(R.id.spol);
        final TextView webInfo = (TextView) findViewById(R.id.webStranica);
        ImageView slikaInfo = (ImageView) findViewById(R.id.slikaInfo);
        final TextView biografijaInfo = (TextView) findViewById(R.id.biografija);

        nazivInfo.setText(getIntent().getStringExtra("naziv"));
        rodjenjeInfo.setText(getIntent().getStringExtra("rodjenje") + " - ");
        smrtInfo.setText(getIntent().getStringExtra("smrt"));
        if(smrtInfo.getText().equals("0")){
            smrtInfo.setText(" ");
        }
        mjestoInfo.setText(getIntent().getStringExtra("mjesto"));
        spolInfo.setText(getIntent().getStringExtra("spol"));
        webInfo.setText(getIntent().getStringExtra("imdb"));

        biografijaInfo.setText(getIntent().getStringExtra("biografija"));
        slikaInfo.setImageResource(getIntent().getExtras().getInt("slika"));

        View someView = findViewById(R.id.pozadina);
        View root = someView.getRootView();

        if(spolInfo.getText().equals("Musko")){
            root.setBackgroundColor(getResources().getColor(R.color.musko));
        }
        if(spolInfo.getText().equals("Zensko")){

            root.setBackgroundColor(getResources().getColor(R.color.zensko));
        }

        biografijaInfo.setMovementMethod(new ScrollingMovementMethod());


        webInfo.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                String url = webInfo.getText().toString();
                Intent i = new Intent(Intent.ACTION_VIEW);

                if(!isOnline()){
                    Toast.makeText(getApplicationContext(), "Niste konektovani", Toast.LENGTH_LONG).show();
                }
                i.setData(Uri.parse(url));
                if (i.resolveActivity(getPackageManager()) != null && isOnline()) {
                    startActivity(i);
                }
                //startActivity(i);
            }
        });


        Button podijeliDugme = (Button) findViewById(R.id.podijeliDugme);

        podijeliDugme.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                Intent sendIntent = new Intent();
                sendIntent.setAction(Intent.ACTION_SEND);
                sendIntent.putExtra(Intent.EXTRA_TEXT, biografijaInfo.getText());
                sendIntent.setType("text/plain");
                if (sendIntent.resolveActivity(getPackageManager()) != null) {
                    startActivity(sendIntent);
                }
                //startActivity(sendIntent);
            }
        });
    }

    public boolean isOnline() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        return netInfo != null && netInfo.isConnectedOrConnecting();
    }
}
