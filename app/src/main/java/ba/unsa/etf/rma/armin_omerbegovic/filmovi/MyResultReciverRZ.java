package ba.unsa.etf.rma.armin_omerbegovic.filmovi;

import android.os.Bundle;
import android.os.Handler;
import android.os.ResultReceiver;

/**
 * Created by Armin on 29.05.2017..
 */

public class MyResultReciverRZ extends ResultReceiver {
    private MyResultReciverRZ.Receiver mReceiver;
    public MyResultReciverRZ(Handler handler) {
        super(handler);
    }
    public void setReceiver(MyResultReciverRZ.Receiver receiver) {
        mReceiver = receiver;
    }


    /* Deklaracija interfejsa koji će se trebati implementirati */
    public interface Receiver {
        public void onReceiveResult(int resultCode, Bundle resultData);
    }


    @Override
    protected void onReceiveResult(int resultCode, Bundle resultData) {
        if (mReceiver != null) {
            mReceiver.onReceiveResult(resultCode, resultData);
        }
    }
}
