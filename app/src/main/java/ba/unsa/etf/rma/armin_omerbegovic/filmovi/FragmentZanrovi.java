package ba.unsa.etf.rma.armin_omerbegovic.filmovi;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import java.util.ArrayList;

/**
 * Created by Armin on 14.04.2017..
 */

public class FragmentZanrovi extends Fragment {
    public static ArrayList<Zanr> zanrovi;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        /*ArrayList<Zanr> listaZanrova = new ArrayList<Zanr>();
        listaZanrova.add(new Zanr("Horror", getResources().getDrawable(R.drawable.horror)));
        listaZanrova.add(new Zanr("Triller", getResources().getDrawable(R.drawable.zhriller)));
        listaZanrova.add(new Zanr("Comedy", getResources().getDrawable(R.drawable.comedy)));
        listaZanrova.add(new Zanr("Science Fiction", getResources().getDrawable(R.drawable.science)));
        listaZanrova.add(new Zanr("Action", getResources().getDrawable(R.drawable.action)));
        listaZanrova.add(new Zanr("Adventure", getResources().getDrawable(R.drawable.adventure)));
        listaZanrova.add(new Zanr("Drama", getResources().getDrawable(R.drawable.drama)));
        listaZanrova.add(new Zanr("Fantasy", getResources().getDrawable(R.drawable.fantasy)));
        listaZanrova.add(new Zanr("Crime", getResources().getDrawable(R.drawable.crime)));
        listaZanrova.add(new Zanr("Western", getResources().getDrawable(R.drawable.western)));
        zanrovi = listaZanrova;*/
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
        return inflater.inflate(R.layout.fragment_zanrovi, container, false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState){
        super.onActivityCreated(savedInstanceState);
        zanrovi = FragmentDetalji.zanrovi;
        ListView lv = (ListView)getView().findViewById(R.id.zanroviListView);
        ZanrAdapter ga = new ZanrAdapter(getActivity(), R.layout.zanr_lista, zanrovi);
        lv.setAdapter(ga);
    }

    @Override
    public void onViewCreated(View v, Bundle savedInstanceState){
        /*ListView lv = (ListView)getView().findViewById(R.id.zanroviListView);
        ZanrAdapter ga = new ZanrAdapter(getActivity(), R.layout.zanr_lista, zanrovi);
        lv.setAdapter(ga);*/
    }
}
