package ba.unsa.etf.rma.armin_omerbegovic.filmovi;

import android.graphics.drawable.Drawable;
import android.media.Image;
import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Armin on 22.03.2017..
 */

public class Glumac implements Parcelable{
    private String imePrezime;
    private String godinaRodjenja;
    private String godinaSmrti;
    private String biografija;
    private String slika;
    private String mjestoRodjenja;
    private String spol;
    private String webStranica;
    private String rating;
    private String id;
    private Boolean bookmark = false;

    public Glumac(){}

    public Glumac(String imePrezime, String godinaRodjenja) {
        this.imePrezime = imePrezime;
        this.godinaRodjenja = godinaRodjenja;
    }

    public Glumac(String id, String imePrezime, String godinaRodjenja, String godinaSmrti, String biografija, String slika, String mjestoRodjenja, String spol, String webStranica, String rating) {
        this.id = id;
        this.imePrezime = imePrezime;
        this.godinaRodjenja = godinaRodjenja;
        this.godinaSmrti = godinaSmrti;
        this.biografija = biografija;
        this.slika = slika;
        this.mjestoRodjenja = mjestoRodjenja;
        this.spol = spol;
        this.webStranica = webStranica;
        this.rating = rating;

    }

    protected Glumac(Parcel in) {
        imePrezime = in.readString();
        godinaRodjenja = in.readString();
        godinaSmrti = in.readString();
        biografija = in.readString();
        slika = in.readString();
        mjestoRodjenja = in.readString();
        spol = in.readString();
        webStranica = in.readString();
        rating = in.readString();
        id = in.readString();
    }

    public static final Creator<Glumac> CREATOR = new Creator<Glumac>() {
        @Override
        public Glumac createFromParcel(Parcel in) {
            return new Glumac(in);
        }

        @Override
        public Glumac[] newArray(int size) {
            return new Glumac[size];
        }
    };

    public String getImePrezime() {
        return imePrezime;
    }

    public void setImePrezime(String imePrezime) {
        this.imePrezime = imePrezime;
    }

    public String getGodinaRodjenja() {
        return godinaRodjenja;
    }

    public void setGodinaRodjenja(String godinaRodjenja) {
        this.godinaRodjenja = godinaRodjenja;
    }

    public String getGodinaSmrti() {
        return godinaSmrti;
    }

    public void setGodinaSmrti(String godinaSmrti) {
        this.godinaSmrti = godinaSmrti;
    }

    public String getBiografija() {
        return biografija;
    }

    public void setBiografija(String biografija) {
        this.biografija = biografija;
    }


    public String getMjestoRodjenja() {
        return mjestoRodjenja;
    }

    public void setMjestoRodjenja(String mjestoRodjenja) {
        this.mjestoRodjenja = mjestoRodjenja;
    }

    public String getSpol() {
        return spol;
    }

    public void setSpol(String spol) {
        this.spol = spol;
    }

    public String getWebStranica() {
        return webStranica;
    }

    public void setWebStranica(String webStranica) {
        this.webStranica = webStranica;
    }

    public String getRating() {
        return rating;
    }

    public void setRating(String rating) {
        this.rating = rating;
    }

    public String getSlika() { return slika; }

    public void setSlika(String slika) { this.slika = slika; }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(imePrezime);
        dest.writeString(godinaRodjenja);
        dest.writeString(godinaSmrti);
        dest.writeString(biografija);
        dest.writeString(slika);
        dest.writeString(mjestoRodjenja);
        dest.writeString(spol);
        dest.writeString(webStranica);
        dest.writeString(rating);
        dest.writeString(id);
    }


    public Boolean getBookmark() {
        return bookmark;
    }

    public void setBookmark(Boolean bookmark) {
        this.bookmark = bookmark;
    }
}


