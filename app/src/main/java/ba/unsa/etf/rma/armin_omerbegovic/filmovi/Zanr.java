package ba.unsa.etf.rma.armin_omerbegovic.filmovi;

import android.graphics.drawable.Drawable;

/**
 * Created by Armin on 22.03.2017..
 */

public class Zanr {
    private String naziv;
    private Drawable slika;

    public Zanr(String naziv) {
        this.naziv = naziv;
    }

    public String getNaziv() {
        return naziv;
    }

    public void setNaziv(String naziv) {
        this.naziv = naziv;
    }

    public Drawable getSlika() {
        return slika;
    }

    public void setSlika(Drawable slika) {
        this.slika = slika;
    }
}
