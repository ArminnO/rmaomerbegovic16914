package ba.unsa.etf.rma.armin_omerbegovic.filmovi;

import android.app.Fragment;
import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Arrays;

import static ba.unsa.etf.rma.armin_omerbegovic.filmovi.GlumacDBOpenHelper.DATABASE_TABLE;
import static ba.unsa.etf.rma.armin_omerbegovic.filmovi.GlumacDBOpenHelper.GLUMAC_WEBID;
import static ba.unsa.etf.rma.armin_omerbegovic.filmovi.GlumacDBOpenHelper.REZISERI_NAZIV;

/**
 * Created by Armin on 11.04.2017..
 */

public class FragmentReziseri extends Fragment {
    private ArrayList<String> reziser;
    public static ArrayList<String> producenti = new ArrayList<>();
    /*private ArrayList<String> reziseriLista = new ArrayList<String>(Arrays.asList("Stivene Spielberg", "James Cameron", "Harvey Weinstein","\n" +
            "Graham King","Joel Silver","David Geffen","\n" +
            "Bob Weinstein","Frank Marshall","\n" +
            "Kathleen Kennedy", "Avi Arad"));*/

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
        View v = inflater.inflate(R.layout.fragment_reziseri, container, false);

        return v;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState){
        super.onActivityCreated(savedInstanceState);
            producenti = FragmentDetalji.producenti;
            ListView lv = (ListView)getView().findViewById(R.id.reziseriListView);
            ArrayAdapter ga = new ArrayAdapter(getActivity(), android.R.layout.simple_list_item_1, producenti);
            lv.setAdapter(ga);
    }
    /*@Override
    public void onViewCreated(View v, Bundle savedInstanceState){
        //super.onActivityCreated(savedInstanceState);
       ///if(getArguments().containsKey("Blista")){
            //reziser = getArguments().getStringArrayList("Blista");
           // ListView lv = (ListView)getView().findViewById(R.id.reziseriListView);
            //GlumacAdapter ga = new GlumacAdapter(getActivity(), R.layout.glumac_lista, reziser);
            //ArrayAdapter ga = new ArrayAdapter(getActivity(),android.R.layout.simple_list_item_1, reziseriLista);
            //lv.setAdapter(ga);
        //}
    }*/

}
