package ba.unsa.etf.rma.armin_omerbegovic.filmovi;

import android.app.Fragment;
import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import java.sql.SQLData;
import java.util.ArrayList;

import static ba.unsa.etf.rma.armin_omerbegovic.filmovi.GlumacDBOpenHelper.DATABASE_TABLE;
import static ba.unsa.etf.rma.armin_omerbegovic.filmovi.GlumacDBOpenHelper.GLUMAC_WEBID;

/**
 * Created by Armin on 14.04.2017..
 */

public class FragmentDetalji extends Fragment implements MyResultReciverRZ.Receiver{
    private Glumac glumac;
    private ArrayList<Glumac> glumci;
    public static ArrayList<String> producenti = new ArrayList<>();
    public static ArrayList<Zanr> zanrovi = new ArrayList<>();

    public static ArrayList<String> glumciID = new ArrayList<>();


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
        View v = inflater.inflate(R.layout.fragment_detalji, container, false);
        if(getArguments() != null && getArguments().containsKey("glumac")){
            glumac = getArguments().getParcelable("glumac");

            String query = glumac.getId().toString();

            Intent intent = new Intent(Intent.ACTION_SYNC, null, getActivity(), MyIntentServiceReziseriZanrovi.class);
            MyResultReciverRZ mReceiver = new MyResultReciverRZ(new Handler());
            mReceiver.setReceiver(FragmentDetalji.this);

            intent.putExtra("id", query);
            intent.putExtra("receiver1", mReceiver);
            getActivity().startService(intent);

            TextView nazivInfo = (TextView) v.findViewById(R.id.imePrezime);
            TextView rodjenjeInfo = (TextView)v.findViewById(R.id.godinaRodjenja);
            TextView smrtInfo = (TextView) v.findViewById(R.id.godinaSmrti);
            TextView mjestoInfo = (TextView) v.findViewById(R.id.mjestoRodjenja);
            TextView spolInfo = (TextView) v.findViewById(R.id.spol);
            final TextView webInfo = (TextView) v.findViewById(R.id.webStranica);
            ImageView slikaInfo = (ImageView) v.findViewById(R.id.slikaInfo);
            final TextView biografijaInfo = (TextView) v.findViewById(R.id.biografija);

            nazivInfo.setText(glumac.getImePrezime());
            rodjenjeInfo.setText(glumac.getGodinaRodjenja() + " - ");
            smrtInfo.setText(glumac.getGodinaSmrti());
            if(smrtInfo.getText().equals("0")){
                smrtInfo.setText(" ");
            }
            mjestoInfo.setText(glumac.getMjestoRodjenja());
            if(glumac.getSpol().equals("2")) {
                spolInfo.setText("Musko");
            }
            if(glumac.getSpol().equals("1")) {
                spolInfo.setText("Zensko");
            }
            webInfo.setText(glumac.getWebStranica());

            biografijaInfo.setText(glumac.getBiografija());
            //slikaInfo.setImageResource(glumac.getSlika());
            Picasso.with(getActivity()).load(glumac.getSlika()).into(slikaInfo);
            View someView = v.findViewById(R.id.pozadina);
            View root = someView.getRootView();

            if(spolInfo.getText().equals("Musko")){
                root.setBackgroundColor(getResources().getColor(R.color.musko));
            }
            if(spolInfo.getText().equals("Zensko")){

                root.setBackgroundColor(getResources().getColor(R.color.zensko));
            }

            webInfo.setOnClickListener(new View.OnClickListener(){
                @Override
                public void onClick(View v){
                    String url = webInfo.getText().toString();
                    Intent i = new Intent(Intent.ACTION_VIEW);
                    i.setData(Uri.parse(url));
                    startActivity(i);
                }
            });

            Button podijeliDugme = (Button) v.findViewById(R.id.podijeliDugme);
            podijeliDugme.setOnClickListener(new View.OnClickListener(){
                @Override
                public void onClick(View v){
                    Intent sendIntent = new Intent();
                    sendIntent.setAction(Intent.ACTION_SEND);
                    sendIntent.putExtra(Intent.EXTRA_TEXT, biografijaInfo.getText());
                    sendIntent.setType("text/plain");
                    startActivity(sendIntent);
                }
            });

            Button bookmark = (Button) v.findViewById(R.id.bookmark);
            bookmark.setOnClickListener(new View.OnClickListener(){
                @Override
                public void onClick(View v){
                    //glumci
                    GlumacDBOpenHelper helper = new GlumacDBOpenHelper(getActivity(), GlumacDBOpenHelper.DATABASE_NAME,null, 1);
                    SQLiteDatabase db = helper.getWritableDatabase();

                    String countQuery = "SELECT  " + GlumacDBOpenHelper.GLUMAC_WEBID + " FROM " + DATABASE_TABLE + " WHERE " + GLUMAC_WEBID + "=" + glumac.getId();
                    Cursor cursor = db.rawQuery(countQuery, null);

                    int broj = cursor.getCount();
                    cursor.close();

                    if(broj == 0) {
                        ContentValues novi = new ContentValues();
                        novi.put(GlumacDBOpenHelper.GLUMAC_IME, glumac.getImePrezime());
                        novi.put(GlumacDBOpenHelper.GLUMAC_RODJENJE, glumac.getGodinaRodjenja());
                        novi.put(GlumacDBOpenHelper.GLUMAC_SMRT, glumac.getGodinaSmrti());
                        novi.put(GlumacDBOpenHelper.GLUMAC_BIOGRAFIJA, glumac.getBiografija());
                        novi.put(GlumacDBOpenHelper.GLUMAC_SLIKA, glumac.getSlika());
                        novi.put(GlumacDBOpenHelper.GLUMAC_MJESTO, glumac.getMjestoRodjenja());
                        novi.put(GlumacDBOpenHelper.GLUMAC_SPOL, glumac.getSpol());
                        novi.put(GlumacDBOpenHelper.GLUMAC_WEB, glumac.getWebStranica());
                        novi.put(GlumacDBOpenHelper.GLUMAC_RATING, glumac.getRating());
                        novi.put(GlumacDBOpenHelper.GLUMAC_WEBID, glumac.getId());


                        Toast.makeText(getActivity(), "Glumac je bookmarkovan", Toast.LENGTH_LONG).show();
                        db.insert(GlumacDBOpenHelper.DATABASE_TABLE, null, novi);
                    }
                    else {
                        ContentValues delete = new ContentValues();
                        String where = GlumacDBOpenHelper.GLUMAC_WEBID + "=" + glumac.getId();
                        String whereArgs[] = null;
                        db.delete(GlumacDBOpenHelper.DATABASE_TABLE, where, whereArgs);

                        Toast.makeText(getActivity(), "Glumac je unbookmark-ovan", Toast.LENGTH_LONG).show();
                    }
                    //reziseri
                    GlumacDBOpenHelper helper1 = new GlumacDBOpenHelper(getActivity(), GlumacDBOpenHelper.DATABASE_NAME, null, 1);
                    SQLiteDatabase db1 = helper1.getWritableDatabase();
                    producenti = MyIntentServiceReziseriZanrovi.rez;
                    for(int i = 0; i < producenti.size(); i++) {
                        String ime = producenti.get(i);
                        String countQuery1 = "SELECT " + GlumacDBOpenHelper.REZISERI_NAZIV + " FROM " + GlumacDBOpenHelper.DATABASE_TABLE2 + " WHERE " + GlumacDBOpenHelper.REZISERI_NAZIV + "='" + ime+"'";
                        Cursor cursor1 = db1.rawQuery(countQuery1, null);
                        int broj1 = cursor1.getCount();
                        cursor1.close();
                        if (broj1 == 0) {
                            ContentValues novi = new ContentValues();
                            novi.put(GlumacDBOpenHelper.REZISERI_NAZIV, producenti.get(i));
                            Log.d("Ime: ", ime);
                            db1.insert(GlumacDBOpenHelper.DATABASE_TABLE2, null, novi);
                        }
                    }
                    //zanrovi
                    GlumacDBOpenHelper helper2 = new GlumacDBOpenHelper(getActivity(), GlumacDBOpenHelper.DATABASE_NAME, null, 1);
                    SQLiteDatabase db2 = helper2.getWritableDatabase();
                    zanrovi = MyIntentServiceReziseriZanrovi.rez2;
                    for(Zanr z : zanrovi){
                        String ime = z.getNaziv();
                        String countQuery2 = "SELECT " + GlumacDBOpenHelper.ZANR_NAZIV + " FROM " + GlumacDBOpenHelper.DATABASE_TABLE3 + " WHERE " + GlumacDBOpenHelper.ZANR_NAZIV + "='" + ime+"'";
                        Cursor cursor2 = db1.rawQuery(countQuery2, null);
                        int broj2 = cursor2.getCount();
                        cursor2.close();
                        if (broj2 == 0) {
                            ContentValues novi = new ContentValues();
                            novi.put(GlumacDBOpenHelper.ZANR_NAZIV, z.getNaziv());
                            novi.put(GlumacDBOpenHelper.ZANR_SLIKA, "SLika");
                            Log.d("Ime: ", ime);
                            db2.insert(GlumacDBOpenHelper.DATABASE_TABLE3, null, novi);
                        }
                    }

                }
            });
        }
        return v;
    }


    @Override
    public void onReceiveResult(int resultCode, Bundle resultData) {

        switch (resultCode){
            case MyIntentService.STATUS_RUNNING:
                Toast.makeText(getActivity(), "Service running", Toast.LENGTH_LONG);
                break;

            case MyIntentService.STATUS_FINISHED:
                ArrayList<String> rezultati1;
                rezultati1 = resultData.getStringArrayList("resultProducer");
                Toast.makeText(getActivity(), "Ucitani reziseri i zanrovi", Toast.LENGTH_LONG).show();
                break;

            case MyIntentService.STATUS_ERROR:
                String error = resultData.getString(Intent.EXTRA_TEXT);
                Toast.makeText(getActivity(), error, Toast.LENGTH_LONG).show();
        }
    }
}
