package ba.unsa.etf.rma.armin_omerbegovic.filmovi;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.Arrays;

public class ReziseriActivity extends AppCompatActivity {
    private ArrayList<String> reziseriLista = new ArrayList<String>();

    Boolean siriL = false;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reziseri);

        /*ListView listaRezisera = (ListView) findViewById(R.id.reziseriListView);
        ArrayList<String> unosi = new ArrayList<String>(Arrays.asList("Stivene Spielberg", "James Cameron", "Harvey Weinstein","\n" +
                "Graham King","Joel Silver","David Geffen","\n" +
                "Bob Weinstein","Frank Marshall","\n" +
                "Kathleen Kennedy", "Avi Arad"));

        final ArrayAdapter<String> adapter;
        adapter = new ArrayAdapter <String>(this,android.R.layout.simple_list_item_1, unosi);
        listaRezisera.setAdapter(adapter);


        Button glumciDugme = (Button) findViewById(R.id.glumci);
        Button zanroviDugme = (Button) findViewById(R.id.zanrovi);

        glumciDugme.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                Intent myIntent = new Intent(ReziseriActivity.this, GlumciActivity.class);
                ReziseriActivity.this.startActivity(myIntent);
            }
        });

        zanroviDugme.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                Intent myIntent = new Intent(ReziseriActivity.this, ZanroviActivity.class);
                ReziseriActivity.this.startActivity(myIntent);
            }
        });*/

        final android.app.FragmentManager fm = getFragmentManager();
        FrameLayout ldetalji = (FrameLayout) findViewById(R.id.mjestoDugmici);

        if (ldetalji != null) {
            siriL = true;
            FragmentDugmici fd;

            fd = (FragmentDugmici) fm.findFragmentById(R.id.mjestoDugmici);
            if (fd == null) {
                fd = new FragmentDugmici();
                fm.beginTransaction().replace(R.id.mjestoDugmici, fd).commit();
            }
        }
        FragmentReziseri fl = (FragmentReziseri) fm.findFragmentByTag("Lista");
        if (fl == null) {
            fl = new FragmentReziseri();
            Bundle argumenti = new Bundle();
            argumenti.putStringArrayList("Blista", reziseriLista);
            fl.setArguments(argumenti);
            fm.beginTransaction().replace(R.id.mjestoListaReziseri, fl, "Lista").commit();
        } else {
            fm.popBackStack(null, android.app.FragmentManager.POP_BACK_STACK_INCLUSIVE);
        }
    }
}
